import {Group as LayerGroup} from "ol/layer.js";
import differenceJS from "../../../src/shared/js/utils/differenceJS";
import sortBy from "../../../src/shared/js/utils/sortBy";
import store from "../../../src/app-store";
/**
 * Collects all visible ol layers, including layers of groups.
 * @param {Boolean} [printMapMarker=false] whether layer "markerPoint" should be filtered out
 * @returns {void}
 */
function getVisibleLayer (printMapMarker = false) {
    let visibleLayerList = getVisibleLayerList(printMapMarker);
    const groupedLayers = getGroupedLayers(visibleLayerList);

    if (groupedLayers.length > 0) {
        visibleLayerList = visibleLayerList.filter(layer => {
            return !(layer instanceof LayerGroup);
        });
        groupedLayers.forEach(groupedLayer => {
            groupedLayer.getLayers().forEach(gLayer => {
                // layer opacity is only set for printing and later it is reverted
                gLayer.setOpacity(groupedLayer.getOpacity());
                visibleLayerList.push(gLayer);
            });
        });
    }

    if (visibleLayerList.length) {
        checkLayersInResolution(visibleLayerList);
    }
}

/**
 * Gets grouped layers.
 * @param {Object} visibleLayerList The list of visible layers from the "2D" map.
 * @returns {Object} {groupedLayers} layers that are instances of `LayerGroup`
 */
function getGroupedLayers (visibleLayerList) {
    let groupedLayers = null;

    groupedLayers = visibleLayerList.filter(layer => {
        return layer instanceof LayerGroup;
    });

    return groupedLayers;
}

/**
 * Gets visible layer list.
 * @param {Boolean} [printMapMarker=false] whether layer "markerPoint" should be filtered out
 * @returns {Object} {visibleLayerList} The list of visible layers from the "2D" map.
 */
function getVisibleLayerList (printMapMarker = false) {
    const layers = mapCollection.getMap("2D").getLayers(),
        visibleLayerList = typeof layers?.getArray !== "function" ? [] : layers.getArray().filter(layer => {
            return layer.getVisible() === true &&
                (
                    layer.get("name") !== "markerPoint" || printMapMarker
                );
        });

    return visibleLayerList;
}

/**
 * Layer opacity is reverted after closing print map tab.
 * @param {Boolean} [printMapMarker=false] whether layer "markerPoint" should be filtered out
 * @returns {void}
 */
function revertLayerOpacity (printMapMarker = false) {
    const visibleLayerList = this.getVisibleLayerList(printMapMarker),
        groupedLayers = this.getGroupedLayers(visibleLayerList);

    if (groupedLayers.length > 0) {
        groupedLayers.forEach(groupedLayer => {
            groupedLayer.getLayers().forEach(gLayer => {
                gLayer.setOpacity(1);
            });
        });
    }
}

/**
 * checks whether the layers may be displayed or printed at the selected scale.
 * @param {array} visibleLayerList with visible layer
 * @returns {void}
 */
function checkLayersInResolution (visibleLayerList) {
    const resoByMaxScale = store.getters["Maps/getResolutionByScale"](store.getters["Modules/FloodRiskManagement/currentScale"], "max"),
        resoByMinScale = store.getters["Maps/getResolutionByScale"](store.getters["Modules/FloodRiskManagement/currentScale"], "min"),
        newVisibleLayer = [];

    visibleLayerList.forEach(layer => {
        if (resoByMaxScale <= layer.getMaxResolution() && resoByMinScale >= layer.getMinResolution()) {
            newVisibleLayer.push(layer);
        }
    });
    sortVisibleLayerListByZindex(newVisibleLayer);
}

/**
 * sorts the visible layer list by zIndex from layer
 * layers with undefined zIndex come to the beginning of array
 * @param {array} visibleLayerList with visible layer
 * @returns {void}
 */
function sortVisibleLayerListByZindex (visibleLayerList) {
    const visibleLayerListWithZIndex = visibleLayerList.filter(layer => {
            return layer.getZIndex() !== undefined;
        }),
        visibleLayerListWithoutZIndex = differenceJS(visibleLayerList, visibleLayerListWithZIndex);

    visibleLayerListWithoutZIndex.push(sortBy(visibleLayerListWithZIndex, (layer) => layer.getZIndex()));
    store.dispatch("Modules/FloodRiskManagement/setVisibleLayerList", [].concat(...visibleLayerListWithoutZIndex));
}

export default {getVisibleLayer, getGroupedLayers, getVisibleLayerList, revertLayerOpacity};
