/**
 * User type definition
 * @typedef {Object} state
 * @property {String} activeTab the current active tab
 */
const state = {
    activeTabId: "info"
};

export default state;
